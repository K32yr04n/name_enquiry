package com.stablebuild.nameenquiry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NameenquiryApplication {

	public static void main(String[] args) {
		SpringApplication.run(NameenquiryApplication.class, args);
	}

}
