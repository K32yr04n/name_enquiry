package com.stablebuild.nameenquiry;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HttpRequestService {

	private static final Logger LOG = LoggerFactory.getLogger(HttpRequestService.class);
	
	@Value("${name.enquiry.server.url}")
	private String serverUrl;
	
	public String getCustomerNameWithBankAndAccountNo(String accNo, String bankName) throws URISyntaxException
	{
		RestTemplate restTemplate = new RestTemplate();
		String bankCode = getBankCode(bankName);
		LOG.info("Bank Code : " + bankCode);
		String payload = getRequestPayload(accNo, bankCode);
		final String url =  serverUrl + payload;
	    URI uri = new URI(url);
	    ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
	    LOG.info("Retrieved customer name : " + result.getBody());
		return result.getBody();
	}

	private String getRequestPayload(String acc, String bankCode)
	{
		String bank = String.format("%-"+4+"s",bankCode).replaceAll(" ","X");
		LOG.debug("Bank : " + bank);
		String payload = bank+acc;
		LOG.debug("Payload : " + payload);
		return payload;
	}

	private String getBankCode(String bankName) {
		if(null != bankName && !("".equalsIgnoreCase(bankName)))
			if( null != map.get(bankName) 
				&& !("".equalsIgnoreCase(map.get(bankName))))
					return map.get(bankName);
		return bankName;
	}
	
	private static Map<String, String> map = new HashMap<>();
	
	static {
		map.put("ACCESS","1");
		map.put("MAINSTREET","2");
		map.put("DIAMOND","34");
		map.put("KEYSTONE","3");
		map.put("ECO","4");
		map.put("FIDELITY", "5");
		map.put("FCMB","7");
		map.put("FIRST","8");
		map.put("GTB","9");
		map.put("SKYE","12");
		map.put("ENTERPRISE","13");
		map.put("STERLING","15");
		map.put("STANDARD","47");
		map.put("STANBIC","79");
		map.put("UBA","16");
		map.put("UNITY","17");
		map.put("WEMA","18");
		map.put("UNION","20");
		map.put("ZENITH","24");
		map.put("HERITAGE","59");
	}

}
