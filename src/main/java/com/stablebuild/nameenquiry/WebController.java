package com.stablebuild.nameenquiry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class WebController {

	@Autowired HttpRequestService service;
	
	private static final Logger LOG = LoggerFactory.getLogger(WebController.class);
	
	@GetMapping("/")
	public String home(Model model) {
		return "index";
	}
	
	@PostMapping("/search")
	public String search(@RequestParam("acc") String accNo, @RequestParam("bank") String bankName,
			RedirectAttributes redirectAttributes) {
		try
		{
			LOG.info("Account number : " +accNo);
			LOG.info("Bank Name : " + bankName);
			String apiResponse = service.getCustomerNameWithBankAndAccountNo(accNo, bankName);
			String response = "<p><em>" + bankName + ", " + accNo + "</em><br/><strong>" + apiResponse +"</strong></p>";
			redirectAttributes.addFlashAttribute("success_msg", response);
		}
		catch(Exception e)
		{
			LOG.error("Exception while processing name enquiry request...", e);
			redirectAttributes.addFlashAttribute("error_msg", "Something went wrong. Your request could not be completed.");
		}
		return "redirect:/";
	}
}
